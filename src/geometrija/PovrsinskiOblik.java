package geometrija;

import java.awt.Color;
import java.awt.Graphics;

public abstract class PovrsinskiOblik extends Oblik {
	private Color bojaUnutrasnjosti = Color.WHITE;
	private String bojaUnutrasnjostiString = "bela";

	public abstract void popuni(Graphics g);

	public Color getBojaUnutrasnjosti() {
		return bojaUnutrasnjosti;
	}

	public void setBojaUnutrasnjosti(Color bojaUnutrasnjosti) {
		this.bojaUnutrasnjosti = bojaUnutrasnjosti;
	}

	public String getBojaUnutrasnjostiString() {
		return bojaUnutrasnjostiString;
	}

	public void setBojaUnutrasnjostiString(String bojaUnutrasnjostiString) {
		this.bojaUnutrasnjostiString = bojaUnutrasnjostiString;
	}

}
