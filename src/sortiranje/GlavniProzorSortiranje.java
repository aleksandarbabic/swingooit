package sortiranje;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import geometrija.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;

import java.awt.FlowLayout;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.awt.event.ActionEvent;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.SystemColor;

public class GlavniProzorSortiranje extends JFrame {

	private JPanel pnlGlavni;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					GlavniProzorSortiranje frame = new GlavniProzorSortiranje();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GlavniProzorSortiranje() {
		setBackground(SystemColor.text);
		setTitle("Zadatak sortiranje - Aleksandar Babić - IT53/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		setLocationRelativeTo(null);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlGlavni);
		pnlGlavni.setLayout(new BorderLayout(0, 0));

		List<Kvadrat> listaKvadrata = new ArrayList<Kvadrat>(); // KREIRANJE
																// GLAVNE LISTE
		DefaultListModel dlm = new DefaultListModel();
		JPanel pnlJug = new JPanel();
		pnlGlavni.add(pnlJug, BorderLayout.SOUTH);
		pnlJug.setLayout(new BorderLayout(0, 0));

		JLabel lblAleksandarBabiIt = new JLabel("Aleksandar Babić IT53/2015");
		lblAleksandarBabiIt.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAleksandarBabiIt.setEnabled(false);
		lblAleksandarBabiIt.setHorizontalAlignment(SwingConstants.LEFT);
		pnlJug.add(lblAleksandarBabiIt);

		JPanel pnlCentar = new JPanel();
		pnlGlavni.add(pnlCentar, BorderLayout.CENTER);
		GridBagLayout gbl_pnlCentar = new GridBagLayout();
		gbl_pnlCentar.columnWidths = new int[] { 0, 0, 0 };
		gbl_pnlCentar.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_pnlCentar.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_pnlCentar.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		pnlCentar.setLayout(gbl_pnlCentar);

		JLabel lblTrenutnoStanjeU = new JLabel("Trenutno stanje u listi : ");
		lblTrenutnoStanjeU.setFont(new Font("Segoe UI", Font.BOLD, 12));
		lblTrenutnoStanjeU.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblTrenutnoStanjeU = new GridBagConstraints();
		gbc_lblTrenutnoStanjeU.anchor = GridBagConstraints.WEST;
		gbc_lblTrenutnoStanjeU.insets = new Insets(0, 0, 5, 5);
		gbc_lblTrenutnoStanjeU.gridx = 0;
		gbc_lblTrenutnoStanjeU.gridy = 1;
		pnlCentar.add(lblTrenutnoStanjeU, gbc_lblTrenutnoStanjeU);

		JButton btnOcisti = new JButton("Očisti listu kvadrata");
		btnOcisti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!listaKvadrata.isEmpty()) {
					dlm.removeAllElements();
					listaKvadrata.clear();
				} else {
					JOptionPane.showMessageDialog(getParent(), "Lista je već prazna!", "Greška",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		GridBagConstraints gbc_btnOcisti = new GridBagConstraints();
		gbc_btnOcisti.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnOcisti.insets = new Insets(0, 0, 5, 0);
		gbc_btnOcisti.gridx = 1;
		gbc_btnOcisti.gridy = 1;
		pnlCentar.add(btnOcisti, gbc_btnOcisti);

		JScrollPane scrollLista = new JScrollPane();
		GridBagConstraints gbc_scrollLista = new GridBagConstraints();
		gbc_scrollLista.gridwidth = 2;
		gbc_scrollLista.fill = GridBagConstraints.BOTH;
		gbc_scrollLista.gridheight = 3;
		gbc_scrollLista.gridx = 0;
		gbc_scrollLista.gridy = 2;
		pnlCentar.add(scrollLista, gbc_scrollLista);
		JList lstKvadrati = new JList();
		lstKvadrati.setModel(dlm);

		scrollLista.setViewportView(lstKvadrati);

		JButton btnDodajKvadrat = new JButton("Dodaj kvadrat u listu");
		btnDodajKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JTextField xKordinata = new JTextField();
				JTextField yKordinata = new JTextField();
				JTextField duzinaStr = new JTextField();
				JComboBox cbxBojaUnutrasnjosti = new JComboBox();
				cbxBojaUnutrasnjosti.setModel(new DefaultComboBoxModel(
						new String[] { "bela", "crna", "plava", "crvena", "zelena", "zuta", "pink" }));
				JComboBox cbxBojaIvice = new JComboBox();
				cbxBojaIvice.setModel(new DefaultComboBoxModel(
						new String[] { "crna", "bela", "plava", "crvena", "zelena", "zuta", "pink" }));
				final JComponent[] unosi = new JComponent[] { new JLabel("X koordinata:"), xKordinata,
						new JLabel("Y koordinata:"), yKordinata, new JLabel("Dužina stranice:"), duzinaStr,
						new JLabel("Odaberite boju unutrašnjosti:"), cbxBojaUnutrasnjosti,
						new JLabel("Odaberite boju ivice:"), cbxBojaIvice

				};
				int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Dodaj kvadrat u listu",
						JOptionPane.PLAIN_MESSAGE);
				if (rezultat == JOptionPane.OK_OPTION) {
					if (!xKordinata.getText().isEmpty() && !yKordinata.getText().isEmpty()
							&& !duzinaStr.getText().isEmpty()) {
						if (Integer.parseInt(duzinaStr.getText()) > 0) {
							try {
								Kvadrat kvTmp = new Kvadrat(
										new Tacka(Integer.parseInt(xKordinata.getText()),
												Integer.parseInt(yKordinata.getText())),
										Integer.parseInt(duzinaStr.getText()),
										cbxBojaIvice.getSelectedItem().toString(),
										cbxBojaUnutrasnjosti.getSelectedItem().toString());
								dlm.addElement(kvTmp);
								listaKvadrata.add(kvTmp);
								System.out.println(Arrays.toString(listaKvadrata.toArray()));
								System.out.println("Debug unosi :  " + xKordinata.getText() + ", "
										+ yKordinata.getText() + ", " + duzinaStr.getText());
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(), "Vrednosti moraju biti celi brojevi!",
										"Greška", JOptionPane.ERROR_MESSAGE);
							}
						} else {
							JOptionPane.showMessageDialog(getParent(), "Vrednosti moraju biti veće od nule!", "Greška",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(getParent(), "Vrednosti ne smeju biti prazne!", "Greška",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					System.out.println("Dijalog otkazan " + rezultat);
				}
			}
		});
		btnDodajKvadrat.setToolTipText("Otvara dijalog za dodavanje kvadrata u listu");
		GridBagConstraints gbc_btnDodajKvadrat = new GridBagConstraints();
		gbc_btnDodajKvadrat.fill = GridBagConstraints.BOTH;
		gbc_btnDodajKvadrat.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodajKvadrat.gridx = 0;
		gbc_btnDodajKvadrat.gridy = 0;
		pnlCentar.add(btnDodajKvadrat, gbc_btnDodajKvadrat);

		JButton btnSortirajListuKvadrata = new JButton("Sortiraj listu kvadrata");
		btnSortirajListuKvadrata.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!listaKvadrata.isEmpty()) {

					Collections.sort(listaKvadrata, new Comparator<Kvadrat>() {

						@Override
						public int compare(Kvadrat o1, Kvadrat o2) {
							return o1.povrsina() - o2.povrsina();
						}

					});

					dlm.removeAllElements();
					Iterator<Kvadrat> iteratorListe = listaKvadrata.iterator();
					while (iteratorListe.hasNext()) {
						dlm.addElement(iteratorListe.next());
					}
				} else {
					JOptionPane.showMessageDialog(getParent(), "Nije moguće sortirati, lista je prazna!", "Greška",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		GridBagConstraints gbc_btnSortirajListuKvadrata = new GridBagConstraints();
		gbc_btnSortirajListuKvadrata.fill = GridBagConstraints.BOTH;
		gbc_btnSortirajListuKvadrata.insets = new Insets(0, 0, 5, 0);
		gbc_btnSortirajListuKvadrata.gridx = 1;
		gbc_btnSortirajListuKvadrata.gridy = 0;
		pnlCentar.add(btnSortirajListuKvadrata, gbc_btnSortirajListuKvadrata);

	}

}
