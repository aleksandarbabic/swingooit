package crtanje;

import java.awt.Graphics;
import javax.swing.JPanel;
import geometrija.*;
import stek.GlavniProzorStek;

public class PanelZaCrtanje extends JPanel {

	@Override
	protected void paintComponent(Graphics g) {

		super.paintComponent(g);
		for (Oblik obl : GlavniProzorCrtanje.glavniStack) {
			obl.crtajSe(g);
			if (obl instanceof Krug) {
				Krug tmp = (Krug) obl;
				tmp.popuni(g);
			}
			if (obl instanceof Kvadrat) {
				Kvadrat tmp = (Kvadrat) obl;
				tmp.popuni(g);
			}
			if (obl instanceof Pravougaonik) {
				Pravougaonik tmp = (Pravougaonik) obl;
				tmp.popuni(g);
			}
		}
	}

}
