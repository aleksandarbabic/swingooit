package crtanje;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import geometrija.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import java.awt.GridLayout;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.Point;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.ButtonGroup;
import net.miginfocom.swing.MigLayout;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.UIManager;
import java.awt.SystemColor;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collections;
import java.util.Stack;
import java.util.concurrent.TimeUnit;
import java.awt.event.ItemEvent;
import javax.swing.JColorChooser;
import javax.swing.JToggleButton;
import javax.swing.event.ChangeListener;

import org.omg.IOP.TAG_MULTIPLE_COMPONENTS;

import javax.swing.event.ChangeEvent;
import java.awt.event.WindowStateListener;
import java.awt.event.WindowEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

public class GlavniProzorCrtanje extends JFrame {

	private JPanel pnlGlavni;

	/**
	 * Globalne varijable.
	 */
	public static Stack<Oblik> glavniStack = new Stack<Oblik>();
	private int x;
	private int y;
	private int brKlikova;
	private Oblik noviOblik = null;
	private Oblik selektovan = null;
	private boolean selektovnBtn;
	private int selektovanIndex;
	private final ButtonGroup grupaManipulacijaOblicima = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					GlavniProzorCrtanje frame = new GlavniProzorCrtanje();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	// START PANELI i Komponente
	JPanel pnlOpcije = new JPanel();
	PanelZaCrtanje pnlCrtanje = new PanelZaCrtanje();
	JPanel pnlInfo = new JPanel();

	JButton pckBojaUnutrasnjosti = new JButton("");
	JButton pckBojaIvice = new JButton("");
	JToggleButton tglbtnSelektuj = new JToggleButton("Selektuj");

	JLabel lblUnutranjost = new JLabel("Unutrašnjost");
	JLabel lblIvica = new JLabel("Ivica");
	// END PANELI

	// START Fieldovi za izmene
	JTextField xKordinata = new JTextField();
	JTextField yKordinata = new JTextField();
	JTextField xKrajnjaKordinata = new JTextField();
	JTextField yKrajnjaKordinata = new JTextField();
	JTextField poluPrecnik = new JTextField();
	JTextField duzinaStr = new JTextField();
	JTextField visinaStr = new JTextField();
	JButton btnBojaIvicaDijalog = new JButton("");
	JButton btnBojaUnutrasnjostDijalog = new JButton("");
	Color odabranaBojaIvicaDijalog = null;
	Color odabranaBojaUnutrasnjostDijalog = null;
	private final JButton btnObrisiSve = new JButton("Obriši SVE");

	// END Fieldovi za izmene
	public GlavniProzorCrtanje() {

		setBackground(SystemColor.text);

		setTitle("Zadatak crtanje - Aleksandar Babić - IT53/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 754, 544);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		pnlGlavni.setLayout(new BorderLayout(0, 0));
		setContentPane(pnlGlavni);

		pnlGlavni.add(pnlOpcije, BorderLayout.NORTH);

		pckBojaIvice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pckBojaIvice.setBackground(odaberiBoju(pckBojaIvice.getBackground()));
			}
		});

		JComboBox cbxOblik = new JComboBox();
		cbxOblik.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				switch (cbxOblik.getSelectedItem().toString()) {
				case "Tačka": {
					pckBojaUnutrasnjosti.setEnabled(false);
					lblUnutranjost.setEnabled(false);
					break;
				}
				case "Linija": {
					pckBojaUnutrasnjosti.setEnabled(false);
					lblUnutranjost.setEnabled(false);
					break;
				}
				default: {
					pckBojaUnutrasnjosti.setEnabled(true);
					lblUnutranjost.setEnabled(true);
					break;
				}

				}
			}
		});

		JLabel lblOdaberiOblik = new JLabel("Oblik:");
		lblOdaberiOblik.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblOdaberiOblik.setVerticalAlignment(SwingConstants.TOP);
		lblOdaberiOblik.setHorizontalAlignment(SwingConstants.LEFT);
		lblUnutranjost.setEnabled(false);
		pnlOpcije.setLayout(new MigLayout("",
				"[31px][106px][71px][71px][76px][76px][][62.00,grow][90.00px,right][right]", "[50px][16px]"));
		pnlOpcije.add(lblOdaberiOblik, "cell 0 0,growx,aligny center");
		cbxOblik.setModel(
				new DefaultComboBoxModel(new String[] { "Tačka", "Linija", "Krug", "Kvadrat", "Pravougaonik" }));
		pnlOpcije.add(cbxOblik, "cell 1 0,growx,aligny center");

		pckBojaUnutrasnjosti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				pckBojaUnutrasnjosti.setBackground(odaberiBoju(pckBojaUnutrasnjosti.getBackground()));
			}
		});
		pckBojaUnutrasnjosti.setEnabled(false);
		pckBojaUnutrasnjosti.setBackground(SystemColor.text);
		pnlOpcije.add(pckBojaUnutrasnjosti, "cell 2 0,grow");
		pckBojaIvice.setBackground(SystemColor.controlText);
		pnlOpcije.add(pckBojaIvice, "cell 3 0,grow");

		JButton btnModifikuj = new JButton("Modifikuj");
		grupaManipulacijaOblicima.add(btnModifikuj);
		btnModifikuj.setBackground(Color.WHITE);
		btnBojaIvicaDijalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBojaIvicaDijalog.setBackground(odaberiBoju(btnBojaIvicaDijalog.getBackground()));

			}
		});

		btnBojaUnutrasnjostDijalog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBojaUnutrasnjostDijalog.setBackground(odaberiBoju(btnBojaUnutrasnjostDijalog.getBackground()));

			}
		});
		btnModifikuj.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (selektovan != null) {
					if (selektovan instanceof Tacka) {
						Tacka tmpTacka = (Tacka) selektovan;
						xKordinata.setText(Integer.toString(tmpTacka.getX()));
						yKordinata.setText(Integer.toString(tmpTacka.getY()));
						btnBojaIvicaDijalog.setBackground(tmpTacka.getBoja());

						final JComponent[] unosi = new JComponent[] { new JLabel("Trenutna X koordinata:"), xKordinata,
								new JLabel("Trenutna Y koordinata:"), yKordinata, new JLabel("Trenutna boja:"),
								btnBojaIvicaDijalog };
						int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Izmeni tačku",
								JOptionPane.PLAIN_MESSAGE);
						if (rezultat == JOptionPane.OK_OPTION) {
							try {
								tmpTacka.setX(Integer.parseInt(xKordinata.getText()));
								tmpTacka.setY(Integer.parseInt(yKordinata.getText()));

								tmpTacka.setBoja(btnBojaIvicaDijalog.getBackground());
								glavniStack.set(selektovanIndex, tmpTacka);
								pnlCrtanje.repaint();
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(),
										"Polja moraju imati cele brojeve i ne smeju biti prazna!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} else {
							System.out.println("Otkazan dijalog - " + rezultat);
						}
					}

					else if (selektovan instanceof Linija) {
						Linija tmpLinija = (Linija) selektovan;
						btnBojaIvicaDijalog.setBackground(tmpLinija.getBoja());
						xKordinata.setText(Integer.toString(tmpLinija.gettPocetna().getX()));
						yKordinata.setText(Integer.toString(tmpLinija.gettPocetna().getY()));
						xKrajnjaKordinata.setText(Integer.toString(tmpLinija.gettKrajnja().getX()));
						yKrajnjaKordinata.setText(Integer.toString(tmpLinija.gettKrajnja().getY()));
						btnBojaIvicaDijalog.setBackground(tmpLinija.getBoja());

						final JComponent[] unosi = new JComponent[] { new JLabel("Trenutna početna X koordinata:"),
								xKordinata, new JLabel("Trenutna početna Y koordinata:"), yKordinata,
								new JLabel("Trenutna krajnja X koordinata:"), xKrajnjaKordinata,
								new JLabel("Trenutna krajnja Y koordinata:"), yKrajnjaKordinata,
								new JLabel("Trenutna boja:"), btnBojaIvicaDijalog };
						int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Izmeni tačku",
								JOptionPane.PLAIN_MESSAGE);
						if (rezultat == JOptionPane.OK_OPTION) {
							try {
								tmpLinija.gettPocetna().setX(Integer.parseInt(xKordinata.getText()));
								tmpLinija.gettPocetna().setY(Integer.parseInt(yKordinata.getText()));
								tmpLinija.gettKrajnja().setX(Integer.parseInt(xKrajnjaKordinata.getText()));
								tmpLinija.gettKrajnja().setY(Integer.parseInt(yKrajnjaKordinata.getText()));

								tmpLinija.setBoja(btnBojaIvicaDijalog.getBackground());
								glavniStack.set(selektovanIndex, tmpLinija);
								pnlCrtanje.repaint();
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(),
										"Polja moraju imati cele brojeve i ne smeju biti prazna!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} else {
							System.out.println("Otkazan dijalog - " + rezultat);
						}
					}

					else if (selektovan instanceof Krug) {
						Krug tmpKrug = (Krug) selektovan;

						xKordinata.setText(Integer.toString(tmpKrug.getCentar().getX()));
						yKordinata.setText(Integer.toString(tmpKrug.getCentar().getY()));
						poluPrecnik.setText(Integer.toString(tmpKrug.getR()));
						btnBojaIvicaDijalog.setBackground(tmpKrug.getBoja());
						btnBojaUnutrasnjostDijalog.setBackground(tmpKrug.getBojaUnutrasnjosti());

						final JComponent[] unosi = new JComponent[] { new JLabel("Trenutna X koordinata centra:"),
								xKordinata, new JLabel("Trenutna Y koordinata centra:"), yKordinata,
								new JLabel("Trenutni poluprečnik:"), poluPrecnik, new JLabel("Trenutna boja ivice:"),
								btnBojaIvicaDijalog, new JLabel("Trenutna boja unutrašnjosti:"),
								btnBojaUnutrasnjostDijalog };
						int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Izmeni krug",
								JOptionPane.PLAIN_MESSAGE);
						if (rezultat == JOptionPane.OK_OPTION) {
							try {
								tmpKrug.getCentar().setX(Integer.parseInt(xKordinata.getText()));
								tmpKrug.getCentar().setY(Integer.parseInt(yKordinata.getText()));
								tmpKrug.setR(Integer.parseInt(poluPrecnik.getText()));
								tmpKrug.setBoja(btnBojaIvicaDijalog.getBackground());
								tmpKrug.setBojaUnutrasnjosti(btnBojaUnutrasnjostDijalog.getBackground());
								glavniStack.set(selektovanIndex, tmpKrug);
								pnlCrtanje.repaint();
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(),
										"Polja moraju imati cele brojeve i ne smeju biti prazna!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} else {
							System.out.println("Otkazan dijalog - " + rezultat);
						}

					}

					else if (selektovan instanceof Pravougaonik) {
						Pravougaonik tmpPravougaonik = (Pravougaonik) selektovan;
						xKordinata.setText(Integer.toString(tmpPravougaonik.getGoreLevo().getX()));
						yKordinata.setText(Integer.toString(tmpPravougaonik.getGoreLevo().getY()));
						duzinaStr.setText(Integer.toString(tmpPravougaonik.getDuzinaStranice()));
						visinaStr.setText(Integer.toString(tmpPravougaonik.getSirina()));
						btnBojaIvicaDijalog.setBackground(tmpPravougaonik.getBoja());
						btnBojaUnutrasnjostDijalog.setBackground(tmpPravougaonik.getBojaUnutrasnjosti());

						final JComponent[] unosi = new JComponent[] { new JLabel("Trenutna X koordinata goreLevo:"),
								xKordinata, new JLabel("Trenutna Y koordinata goreLevo:"), yKordinata,
								new JLabel("Trenutna dužina stranice:"), duzinaStr, new JLabel("Trenutna visina: "),
								visinaStr, new JLabel("Trenutna boja ivice:"), btnBojaIvicaDijalog,
								new JLabel("Trenutna boja unutrašnjosti:"), btnBojaUnutrasnjostDijalog };
						int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Izmeni pravougaonik",
								JOptionPane.PLAIN_MESSAGE);
						if (rezultat == JOptionPane.OK_OPTION) {
							try {
								tmpPravougaonik.getGoreLevo().setX(Integer.parseInt(xKordinata.getText()));
								tmpPravougaonik.getGoreLevo().setY(Integer.parseInt(yKordinata.getText()));
								tmpPravougaonik.setDuzinaStranica(Integer.parseInt(duzinaStr.getText()));
								tmpPravougaonik.setSirina(Integer.parseInt(visinaStr.getText()));
								tmpPravougaonik.setBoja(btnBojaIvicaDijalog.getBackground());
								tmpPravougaonik.setBojaUnutrasnjosti(btnBojaUnutrasnjostDijalog.getBackground());
								glavniStack.set(selektovanIndex, tmpPravougaonik);
								pnlCrtanje.repaint();
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(),
										"Polja moraju imati cele brojeve i ne smeju biti prazna!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} else {
							System.out.println("Otkazan dijalog - " + rezultat);
						}
					}

					else if (selektovan instanceof Kvadrat) {
						Kvadrat tmpKvadrat = (Kvadrat) selektovan;

						xKordinata.setText(Integer.toString(tmpKvadrat.getGoreLevo().getX()));
						yKordinata.setText(Integer.toString(tmpKvadrat.getGoreLevo().getY()));
						duzinaStr.setText(Integer.toString(tmpKvadrat.getDuzinaStranice()));
						btnBojaIvicaDijalog.setBackground(tmpKvadrat.getBoja());
						btnBojaUnutrasnjostDijalog.setBackground(tmpKvadrat.getBojaUnutrasnjosti());

						final JComponent[] unosi = new JComponent[] { new JLabel("Trenutna X koordinata goreLevo:"),
								xKordinata, new JLabel("Trenutna Y koordinata goreLevo:"), yKordinata,
								new JLabel("Trenutna dužina stranice:"), duzinaStr, new JLabel("Trenutna boja ivice:"),
								btnBojaIvicaDijalog, new JLabel("Trenutna boja unutrašnjosti:"),
								btnBojaUnutrasnjostDijalog };
						int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Izmeni kvadrat",
								JOptionPane.PLAIN_MESSAGE);
						if (rezultat == JOptionPane.OK_OPTION) {
							try {
								tmpKvadrat.getGoreLevo().setX(Integer.parseInt(xKordinata.getText()));
								tmpKvadrat.getGoreLevo().setY(Integer.parseInt(yKordinata.getText()));
								tmpKvadrat.setDuzinaStranica(Integer.parseInt(duzinaStr.getText()));
								tmpKvadrat.setBoja(btnBojaIvicaDijalog.getBackground());
								tmpKvadrat.setBojaUnutrasnjosti(btnBojaUnutrasnjostDijalog.getBackground());
								glavniStack.set(selektovanIndex, tmpKvadrat);
								pnlCrtanje.repaint();
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(),
										"Polja moraju imati cele brojeve i ne smeju biti prazna!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
						} else {
							System.out.println("Otkazan dijalog - " + rezultat);
						}
					}

				} else {
					JOptionPane.showMessageDialog(getParent(), "Nije selektovan element za modifikaciju!", "Greška",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		tglbtnSelektuj.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (!tglbtnSelektuj.isSelected()) {
					prodjiKrozStek(x, y);
					pnlCrtanje.repaint();
				}

			}
		});
		pnlOpcije.add(tglbtnSelektuj, "cell 4 0,grow");
		pnlOpcije.add(btnModifikuj, "cell 5 0,grow");

		JButton btnObrisi = new JButton("Obri\u0161i");
		btnObrisi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!glavniStack.isEmpty() && selektovan != null) {

					Object[] odgovori = { "Da", "Ne" };
					int rezultatDijaloga = JOptionPane.showOptionDialog(null,
							"Da li ste sigurni da želite da obrišete element :\n" + selektovan + " ?", "UPOZORENJE",
							JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, odgovori, odgovori[0]);
					if (rezultatDijaloga == JOptionPane.YES_OPTION) {
						glavniStack.remove(selektovanIndex);
						pnlCrtanje.repaint();
					}
				} else
					JOptionPane.showMessageDialog(getParent(), "Nije selektovan element za brisanje!", "Greška",
							JOptionPane.ERROR_MESSAGE);

			}
		});
		grupaManipulacijaOblicima.add(btnObrisi);
		btnObrisi.setBackground(new Color(255, 0, 0));
		pnlOpcije.add(btnObrisi, "cell 8 0,grow");
		btnObrisiSve.setFont(new Font("SansSerif", Font.BOLD, 12));
		btnObrisiSve.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!glavniStack.isEmpty()) {
					Object[] odgovori = { "DA", "NE" };
					int rezultatDijaloga = JOptionPane.showOptionDialog(null,
							"Da li ste sigurni da želite da obrišete SVE elemente?", "UPOZORENJE",
							JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, odgovori, odgovori[0]);
					if (rezultatDijaloga == JOptionPane.YES_OPTION) {
						glavniStack.removeAllElements();
						pnlCrtanje.repaint();
					}
				}
			}
		});
		btnObrisiSve.setBackground(new Color(255, 0, 0));
		pnlOpcije.add(btnObrisiSve, "cell 9 0,grow");
		pnlOpcije.add(lblUnutranjost, "cell 2 1,alignx center,aligny top");
		pnlOpcije.add(lblIvica, "cell 3 1,alignx center,aligny top");
		pnlCrtanje.setBackground(SystemColor.text);

		pnlCrtanje.addMouseListener(new MouseAdapter() {
			@Override

			public void mousePressed(MouseEvent e) {
				x = e.getX();
				y = e.getY();
				if (!tglbtnSelektuj.isSelected()) {
					switch (cbxOblik.getSelectedItem().toString()) {
					case "Tačka": {
						Tacka tmpTacka = new Tacka(x, y);
						tmpTacka.setBoja(pckBojaIvice.getBackground());
						glavniStack.push(tmpTacka);
						pnlCrtanje.repaint();
						debugSteka();
						break;
					}
					case "Linija": {
						Object o = e.getSource();
						if (o instanceof JPanel)
							brKlikova++;
						
						if (brKlikova == 1) {
							noviOblik = new Linija(new Tacka(x, y), new Tacka(0, 0));
						}
						if (brKlikova == 2) {
							Linija tmpLinija = (Linija) noviOblik;
							tmpLinija.settKrajnja(new Tacka(e.getX(), e.getY()));
							tmpLinija.setBoja(pckBojaIvice.getBackground());
							brKlikova = 0;
							glavniStack.push(tmpLinija);
							pnlCrtanje.repaint();
							restartujOblik();
							debugSteka();
						}
						break;
					}

					case "Krug": {
						try {
							int tmpPPrecnik = Integer.parseInt(JOptionPane.showInputDialog("Veličina poluprečnika?"));
							if (tmpPPrecnik > 0) {
								Krug tmpKrug = new Krug(new Tacka(x, y), tmpPPrecnik);
								tmpKrug.setBojaUnutrasnjosti(pckBojaUnutrasnjosti.getBackground());
								tmpKrug.setBoja(pckBojaIvice.getBackground());
								glavniStack.push(tmpKrug);
								pnlCrtanje.repaint();
								debugSteka();
							} else {
								JOptionPane.showMessageDialog(getParent(), "Poluprečnik mora biti veći od nule!",
										"Greška", JOptionPane.ERROR_MESSAGE);
							}
						} catch (NumberFormatException ex) {

							JOptionPane.showMessageDialog(getParent(), "Poluprečnik mora biti ceo broj!", "Greška",
									JOptionPane.ERROR_MESSAGE);
						}
						break;

					}

					case "Kvadrat": {
						try {
							int tmpStranica = Integer.parseInt(JOptionPane.showInputDialog("Dužina stranice?"));
							if (tmpStranica > 0) {
								Kvadrat tmpKvadrat = new Kvadrat(new Tacka(x, y), tmpStranica);
								tmpKvadrat.setBojaUnutrasnjosti(pckBojaUnutrasnjosti.getBackground());
								tmpKvadrat.setBoja(pckBojaIvice.getBackground());
								glavniStack.push(tmpKvadrat);
								pnlCrtanje.repaint();
								debugSteka();
							} else {
								JOptionPane.showMessageDialog(getParent(), "Stranica mora biti veća od nule!", "Greška",
										JOptionPane.ERROR_MESSAGE);
							}
							break;
						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(getParent(), "Stranica mora biti ceo broj!", "Greška",
									JOptionPane.ERROR_MESSAGE);
							break;
						}
					}

					case "Pravougaonik": {
						try {
							JTextField visina = new JTextField();
							JTextField sirina = new JTextField();
							final JComponent[] unosi = new JComponent[] { new JLabel("Visina?"), visina,
									new JLabel("Sirina?"), sirina

							};
							int rezultat = JOptionPane.showConfirmDialog(null, unosi, "Unesi visinu i sirinu",
									JOptionPane.PLAIN_MESSAGE);
							if (rezultat == JOptionPane.OK_OPTION) {
								if (Integer.parseInt(sirina.getText()) > 0 && Integer.parseInt(visina.getText()) > 0) {
									Pravougaonik tmpPravougaonik = new Pravougaonik(new Tacka(x, y),
											Integer.parseInt(sirina.getText()), Integer.parseInt(visina.getText()));
									tmpPravougaonik.setBojaUnutrasnjosti(pckBojaUnutrasnjosti.getBackground());
									tmpPravougaonik.setBoja(pckBojaIvice.getBackground());
									glavniStack.push(tmpPravougaonik);
									pnlCrtanje.repaint();
									debugSteka();
								} else {
									JOptionPane.showMessageDialog(getParent(),
											"Visina i sirina moraju biti veći od nule!", "Greška",
											JOptionPane.ERROR_MESSAGE);
								}
							}
							break;
						} catch (NumberFormatException ex) {
							JOptionPane.showMessageDialog(getParent(), "Visina i sirina moraju biti celi brojevi!",
									"Greška", JOptionPane.ERROR_MESSAGE);
							break;
						}
					}
					}

				} else

					selektovanIndex = prodjiKrozStek(x, y);

			}
		});
		pnlGlavni.add(pnlCrtanje, BorderLayout.CENTER);
		GridBagLayout gbl_pnlCrtanje = new GridBagLayout();
		gbl_pnlCrtanje.columnWidths = new int[] { 0 };
		gbl_pnlCrtanje.rowHeights = new int[] { 0 };
		gbl_pnlCrtanje.columnWeights = new double[] { Double.MIN_VALUE };
		gbl_pnlCrtanje.rowWeights = new double[] { Double.MIN_VALUE };
		pnlCrtanje.setLayout(gbl_pnlCrtanje);

		pnlGlavni.add(pnlInfo, BorderLayout.SOUTH);
		GridBagLayout gbl_pnlInfo = new GridBagLayout();
		gbl_pnlInfo.columnWidths = new int[] { 287, 154, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_pnlInfo.rowHeights = new int[] { 16, 0 };
		gbl_pnlInfo.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, Double.MIN_VALUE };
		gbl_pnlInfo.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		pnlInfo.setLayout(gbl_pnlInfo);

		JLabel lblIme = new JLabel("Aleksandar Babić IT53/2015");
		lblIme.setEnabled(false);
		lblIme.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblIme = new GridBagConstraints();
		gbc_lblIme.insets = new Insets(0, 0, 0, 5);
		gbc_lblIme.anchor = GridBagConstraints.NORTHWEST;
		gbc_lblIme.gridx = 0;
		gbc_lblIme.gridy = 0;
		pnlInfo.add(lblIme, gbc_lblIme);

		JLabel lblXY = new JLabel("");
		lblXY.setHorizontalAlignment(SwingConstants.RIGHT);
		GridBagConstraints gbc_lblXY = new GridBagConstraints();
		gbc_lblXY.fill = GridBagConstraints.BOTH;
		gbc_lblXY.gridx = 9;
		gbc_lblXY.gridy = 0;
		pnlInfo.add(lblXY, gbc_lblXY);
		pnlCrtanje.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				x = e.getX();
				y = e.getY();
				lblXY.setText("X:" + x + "|Y:" + y);

			}
		});

	}

	public void restartujOblik() {
		noviOblik = null;
	}

	public void debugSteka() {
		System.out.println("Detalji o elemntima u steku:");
		for (Oblik obl : glavniStack)
			System.out.println(obl);
	}

	public Color odaberiBoju(Color prethodnaBoja) {
		Color odabranaBoja = JColorChooser.showDialog(this, "Odaberite boju", null);
		if (odabranaBoja != null)
			return odabranaBoja;
		else
			return prethodnaBoja;
	}

	public int prodjiKrozStek(int x, int y) {

		if (selektovan != null) {
			selektovan = null;
			for (Oblik obl : glavniStack) {
				obl.setSelektovan(false);
			}
			pnlCrtanje.repaint();
		}

		for (int i = glavniStack.size() - 1; i >= 0; i--) {
			if (selektovan == null && glavniStack.get(i).sadrzi(x, y)) {
				selektovan = glavniStack.get(i);
				glavniStack.get(i).setSelektovan(true);
				pnlCrtanje.repaint();
				System.out.println("Indeks selektovanog elementa: " + i);
				return i;
			}
		}
		return -1;
	}

}
