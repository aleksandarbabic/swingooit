package stek;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import geometrija.*;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import java.awt.GridLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFormattedTextField;

import java.awt.FlowLayout;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;

import java.util.Arrays;
import java.util.Stack;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.awt.event.ActionEvent;
import javax.swing.JColorChooser;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import java.awt.Font;
import java.awt.SystemColor;

public class GlavniProzorStek extends JFrame {

	private JPanel pnlGlavni;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
					GlavniProzorStek frame = new GlavniProzorStek();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	Stack<Kvadrat> glavniStack = new Stack<Kvadrat>(); // KREIRANJE GLAVNOG
	// STACKA
	DefaultListModel<Oblik> dlm = new DefaultListModel<Oblik>();

	public GlavniProzorStek() {
		setBackground(SystemColor.text);
		setTitle("Zadatak stek - Aleksandar Babić - IT53/2015");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 550, 300);
		setLocationRelativeTo(null);
		pnlGlavni = new JPanel();
		pnlGlavni.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(pnlGlavni);
		pnlGlavni.setLayout(new BorderLayout(0, 0));

		JPanel pnlJug = new JPanel();
		pnlGlavni.add(pnlJug, BorderLayout.SOUTH);
		pnlJug.setLayout(new BorderLayout(0, 0));

		JLabel lblAleksandarBabiIt = new JLabel("Aleksandar Babić IT53/2015");
		lblAleksandarBabiIt.setVerticalAlignment(SwingConstants.BOTTOM);
		lblAleksandarBabiIt.setEnabled(false);
		lblAleksandarBabiIt.setHorizontalAlignment(SwingConstants.LEFT);
		pnlJug.add(lblAleksandarBabiIt);

		JPanel pnlCentar = new JPanel();
		pnlGlavni.add(pnlCentar, BorderLayout.CENTER);
		GridBagLayout gbl_pnlCentar = new GridBagLayout();
		gbl_pnlCentar.columnWidths = new int[] { 0, 0, 0 };
		gbl_pnlCentar.rowHeights = new int[] { 0, 0, 0, 0, 0 };
		gbl_pnlCentar.columnWeights = new double[] { 1.0, 1.0, Double.MIN_VALUE };
		gbl_pnlCentar.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		pnlCentar.setLayout(gbl_pnlCentar);

		JButton btnDodajKvadrat = new JButton("Dodaj kvadrat u stek");
		btnDodajKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JTextField xKordinata = new JTextField();
				JTextField yKordinata = new JTextField();
				JTextField duzinaStr = new JTextField();
				JComboBox cbxBojaUnutrasnjosti = new JComboBox();
				cbxBojaUnutrasnjosti.setModel(new DefaultComboBoxModel(
						new String[] { "bela", "crna", "plava", "crvena", "zelena", "zuta", "pink" }));
				JComboBox cbxBojaIvice = new JComboBox();
				cbxBojaIvice.setModel(new DefaultComboBoxModel(
						new String[] { "crna", "bela", "plava", "crvena", "zelena", "zuta", "pink" }));
				final JComponent[] elementi = new JComponent[] { new JLabel("X koordinata:"), xKordinata,
						new JLabel("Y koordinata:"), yKordinata, new JLabel("Dužina stranice:"), duzinaStr,
						new JLabel("Odaberite boju unutrašnjosti:"), cbxBojaUnutrasnjosti,
						new JLabel("Odaberite boju ivice:"), cbxBojaIvice

				};
				int rezultat = JOptionPane.showConfirmDialog(null, elementi, "Dodaj kvadrat u stek",
						JOptionPane.PLAIN_MESSAGE);
				if (rezultat == JOptionPane.OK_OPTION) {
					if (!xKordinata.getText().isEmpty() && !yKordinata.getText().isEmpty()
							&& !duzinaStr.getText().isEmpty()) {
						if (Integer.parseInt(duzinaStr.getText()) > 0) {
							try {
								Kvadrat kvTmp = new Kvadrat(
										new Tacka(Integer.parseInt(xKordinata.getText()),
												Integer.parseInt(yKordinata.getText())),
										Integer.parseInt(duzinaStr.getText()),
										cbxBojaIvice.getSelectedItem().toString(),
										cbxBojaUnutrasnjosti.getSelectedItem().toString());
								glavniStack.push(kvTmp);
								obrniPrikaz();
								System.out.println(Arrays.toString(glavniStack.toArray()));
								System.out.println("Debug unosi :  " + xKordinata.getText() + ", "
										+ yKordinata.getText() + ", " + duzinaStr.getText());
							} catch (NumberFormatException ex) {
								JOptionPane.showMessageDialog(getParent(), "Vrednosti moraju biti celi brojevi!",
										"Greška", JOptionPane.ERROR_MESSAGE);
							}
						} else {
							JOptionPane.showMessageDialog(getParent(), "Vrednosti moraju biti veće od nule!", "Greška",
									JOptionPane.ERROR_MESSAGE);
						}
					} else {
						JOptionPane.showMessageDialog(getParent(), "Vrednosti ne smeju biti prazne!", "Greška",
								JOptionPane.ERROR_MESSAGE);
					}
				} else {
					System.out.println("Dijalog otkazan " + rezultat);
				}
			}
		});
		btnDodajKvadrat.setHorizontalAlignment(SwingConstants.LEFT);
		btnDodajKvadrat.setToolTipText("Otvara dijalog za dodavanje kvadrata na stek");
		GridBagConstraints gbc_btnDodajKvadrat = new GridBagConstraints();
		gbc_btnDodajKvadrat.fill = GridBagConstraints.BOTH;
		gbc_btnDodajKvadrat.insets = new Insets(0, 0, 5, 5);
		gbc_btnDodajKvadrat.gridx = 0;
		gbc_btnDodajKvadrat.gridy = 0;
		pnlCentar.add(btnDodajKvadrat, gbc_btnDodajKvadrat);

		JButton btnIzuzmiKvadrat = new JButton("Izuzmi kvadrat sa steka");
		btnIzuzmiKvadrat.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!glavniStack.isEmpty()) {
					Kvadrat peekKv = (Kvadrat) glavniStack.peek();
					Object[] opcije = { "Izuzmi", "Odustani" };
					final JComponent[] elementi = new JComponent[] {
							new JLabel("X koordinata: " + peekKv.getGoreLevo().getX()),
							new JLabel("Y koordinata: " + peekKv.getGoreLevo().getY()),
							new JLabel("Dužina stranice: " + peekKv.getDuzinaStranice()),
							new JLabel("Boja ivice: " + peekKv.getBojaString()),
							new JLabel("Boja unutrašnjosti: " + peekKv.getBojaUnutrasnjostiString()),

					};
					int rezultat = JOptionPane.showOptionDialog(null, elementi, "Izuzmi kvadrat sa steka",
							JOptionPane.PLAIN_MESSAGE, JOptionPane.WARNING_MESSAGE, null, opcije, opcije[0]);
					if (rezultat == JOptionPane.OK_OPTION) {
						dlm.removeElement(peekKv);
						System.out.println("Debug obrisan poslednji element stacka : " + glavniStack.pop());

					} else {
						System.out.println("Dijalog otkazan " + rezultat);
					}
				} else {
					JOptionPane.showMessageDialog(getParent(), "Stek je već prazan!", "Greška",
							JOptionPane.ERROR_MESSAGE);
				}
			}
		});
		btnIzuzmiKvadrat.setHorizontalAlignment(SwingConstants.LEFT);
		btnIzuzmiKvadrat.setToolTipText("Otvara dijalog za izuzimanje kvadrata sa steka");
		GridBagConstraints gbc_btnIzuzmiKvadrat = new GridBagConstraints();
		gbc_btnIzuzmiKvadrat.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnIzuzmiKvadrat.insets = new Insets(0, 0, 5, 0);
		gbc_btnIzuzmiKvadrat.gridx = 1;
		gbc_btnIzuzmiKvadrat.gridy = 0;
		pnlCentar.add(btnIzuzmiKvadrat, gbc_btnIzuzmiKvadrat);

		JLabel lblTrenutnoStanjeU = new JLabel("Trenutno stanje u steku : ");
		lblTrenutnoStanjeU.setFont(new Font("Segoe UI", Font.BOLD, 12));
		lblTrenutnoStanjeU.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblTrenutnoStanjeU = new GridBagConstraints();
		gbc_lblTrenutnoStanjeU.anchor = GridBagConstraints.WEST;
		gbc_lblTrenutnoStanjeU.insets = new Insets(0, 0, 5, 5);
		gbc_lblTrenutnoStanjeU.gridx = 0;
		gbc_lblTrenutnoStanjeU.gridy = 1;
		pnlCentar.add(lblTrenutnoStanjeU, gbc_lblTrenutnoStanjeU);

		JScrollPane scrollStek = new JScrollPane();
		GridBagConstraints gbc_scrollStek = new GridBagConstraints();
		gbc_scrollStek.gridwidth = 2;
		gbc_scrollStek.fill = GridBagConstraints.BOTH;
		gbc_scrollStek.gridheight = 3;
		gbc_scrollStek.gridx = 0;
		gbc_scrollStek.gridy = 2;
		pnlCentar.add(scrollStek, gbc_scrollStek);

		JList lstStek = new JList();
		lstStek.setModel(dlm);
		scrollStek.setViewportView(lstStek);
	}

	public void obrniPrikaz() {
		dlm.removeAllElements();
		for (int i = glavniStack.size() - 1; i >= 0; i--) {
			dlm.addElement(glavniStack.get(i));
		}
	}

}
